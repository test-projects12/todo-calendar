import { Component, Input, OnInit } from '@angular/core';

export enum EventTypes {
  celebration = 0,
  meeting = 1,
  other = 2
}

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  @Input() type: EventTypes = EventTypes.other;
  @Input() name = 'Название';
  @Input() money = '';
  @Input() addres: string;
  @Input() time = '00:00';
  @Input() other = 'текст';

  constructor() { }

  ngOnInit(): void {
  }
}
