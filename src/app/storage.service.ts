import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  addEvent(date, event): void {
    const events = localStorage.getItem(date.toString());
    let eventsArray = [];
    if (events) {
      eventsArray = JSON.parse(events);
    }
    eventsArray.push(event);
    localStorage.setItem(date.toString(), JSON.stringify(eventsArray));
  }

  removeEvent(date: Date): void {
    localStorage.removeItem(date.toString());
  }

  getEvents(date: Date): any {
    return JSON.parse(localStorage.getItem(date.toString()));
  }
}
