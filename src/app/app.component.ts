import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { StorageService } from './storage.service';

interface Types {
    name: string;
    code: number;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [ StorageService ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
    date: Date;
    currentEvents: any;
    showForm = false;
    selectedType = {code: 2};
    currentAddres: string;
    currentTime: string;
    currentName: string;
    currentMoney: string;
    currentOther: string;

    types: Types[];

    constructor(private storageService: StorageService) {
        this.types = [
            {
                name: 'celebration',
                code: 0,
            },
            {
                name: 'meeting',
                code: 1,
            },
            {
                name: 'other',
                code: 2,
            },
        ];
    }

    ngOnInit(): void {
        this.date = new Date();
        this.changeDate();
    }

    changeDate(): void {
        this.currentEvents = this.storageService.getEvents(this.date);
    }

    saveForm(): void {
        this.storageService.addEvent(this.date,
            {
                addres: this.currentAddres,
                time: this.currentTime,
                name: this.currentName,
                money: this.currentMoney,
                other: this.currentOther,
                type: this.selectedType.code,
            });
        this.showForm = false;
        this.currentAddres = undefined;
        this.currentTime = undefined;
        this.currentName = undefined;
        this.currentMoney = undefined;
        this.currentOther = undefined;
    }
}
